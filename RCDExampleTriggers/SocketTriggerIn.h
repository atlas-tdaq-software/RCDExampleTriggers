// -*- c++ -*-
// $Id$ 
//
// $Log$
// Revision 1.2  2006/03/10 12:20:10  gcrone
// Rename stopFE to stopEB
//
// Revision 1.1.1.1  2005/06/22 08:22:59  gcrone
// new package
//
// Revision 1.1  2005/05/27 09:03:25  gcrone
// Added HardwareTriggerIn and related Requests
//
//
#ifndef SOCKETTRIGGERIN_H
#define SOCKETTRIGGERIN_H

#include "ROSIO/HardwareTriggerIn.h"

namespace ROS{

   class SocketTriggerIn : public HardwareTriggerIn {
   public:
      virtual void setup(IOManager* iomanager, DFCountedPointer<Config> configuration);
      virtual void stopGathering(const daq::rc::TransitionCmd&);
      virtual void prepareForRun(const daq::rc::TransitionCmd&);

      virtual void clear();
   protected:
      virtual bool waitForTrigger();
   private:
      int m_port;
      int m_listenSocket;
      int m_triggerSocket;
   };
}
#endif
