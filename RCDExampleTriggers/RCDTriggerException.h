// -*- c++ -*-
// $Id$ 

#ifndef RCDTRIGGEREXCEPTION_H
#define RCDTRIGGEREXCEPTION_H

#include "DFExceptions/ROSException.h"
#include <string>

class RCDTriggerException : public ROSException {

public:
  enum ErrorCode {VME_OPEN = 100,
                  VME_INTERRUPTLINK,
                  VME_INTERRUPTUNLINK,
                  VME_INTERRUPTWAIT,
                  VME_CLOSE,
                  CORBO_OPEN,
                  CORBO_CLOSE,
                  SOCKET_CREATE_ERROR,
                  SOCKET_BIND_ERROR,
                  SOCKET_ACCEPT_ERROR,
                  SOCKET_LISTEN_ERROR

  };

  RCDTriggerException(ErrorCode error) ;
  RCDTriggerException(ErrorCode error, std::string description) ;

  RCDTriggerException(ErrorCode error, const ers::Context& context) ;
  RCDTriggerException(ErrorCode error, std::string description, const ers::Context& context) ;
  RCDTriggerException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

protected:
  virtual std::string getErrorString(unsigned int errorId) const;

};

inline
RCDTriggerException::RCDTriggerException(RCDTriggerException::ErrorCode error) 
   : ROSException("RCDTriger",error,getErrorString(error)) { }

inline
RCDTriggerException::RCDTriggerException(RCDTriggerException::ErrorCode error,
		std::string description) 
   : ROSException("RCDTrigger",error,getErrorString(error),description) { }


inline RCDTriggerException::RCDTriggerException(RCDTriggerException::ErrorCode error, const ers::Context& context)
     : ROSException("RCDTrigger", error, getErrorString(error), context) { }
   
inline RCDTriggerException::RCDTriggerException(RCDTriggerException::ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException("RCDTrigger", error, getErrorString(error), description, context) { }

inline RCDTriggerException::RCDTriggerException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException(cause, "RCDTrigger", error, getErrorString(error), description, context) { }


inline std::string RCDTriggerException::getErrorString(unsigned int errorId) const
{
std::string result;    

   switch (errorId) {
    case VME_OPEN:
      result = "Error on VME_Open";
      break;
    case VME_INTERRUPTLINK:
      result = "Error on VME_InterruptLink";
      break;
    case VME_INTERRUPTUNLINK:
      result = "Error on VME_InterruptUnlink";
      break;
    case VME_INTERRUPTWAIT:
      result = "Error on VME_InterruptWait";
      break;
    case VME_CLOSE:
      result = "Error on VME_Close";
      break;
    case CORBO_OPEN:
      result = "Error on CORBO open";
      break;
    case CORBO_CLOSE:
      result = "Error on CORBO close";
      break;
   case SOCKET_CREATE_ERROR:
      result = "Couldn't open a socket ";
      break;
   case SOCKET_BIND_ERROR:
      result = "Couldn't bind a socket ";
      break;
   case SOCKET_ACCEPT_ERROR:
      result = "Couldn't accept a socket ";
      break;
   case SOCKET_LISTEN_ERROR:
      result = "Couldn't set a socket to listen ";
      break;

   default:
      result= "Unspecified error";
      break;
   }
   return(result);
}


#endif
