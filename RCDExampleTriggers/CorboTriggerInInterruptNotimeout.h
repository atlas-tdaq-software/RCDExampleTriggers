// -*- c++ -*-
// $Id: CorboTriggerIn.h 117146 2013-10-09 11:30:27Z gcrone $ 
//
#ifndef CORBOTRIGGERININTERRUPTNOTIMEOUT_H
#define CORBOTRIGGERININTERRUPTNOTIMEOUT_H

#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ROSIO/HardwareTriggerIn.h"

namespace ROS{

   class CorboTriggerInInterruptNotimeout : public HardwareTriggerIn {
   public:
      virtual void setup(IOManager* iomanager, DFCountedPointer<Config> configuration);
      virtual void configure(const daq::rc::TransitionCmd&);
      virtual void unconfigure(const daq::rc::TransitionCmd&);

      virtual void stopDC(const daq::rc::TransitionCmd&);
      virtual void prepareForRun(const daq::rc::TransitionCmd&);

      virtual void clear();

      // CORBO address hardwired ..
      static const unsigned int c_vmeBaseCorbo = 0x700000;

// magic CORBO bit patterns
  enum {
//    (internal) busy enabled, busy latch, front panel input, internal busy out,
//    count triggers, disable external counter clear, pushbutton resets BIM
      INI_CSR1 = 0xc0,
//    (internal) busy disabled, busy latch, front panel input, internal busy out,
//    count triggers, disable external counter clear, pushbutton resets BIM
      DIS_CSR1 = 0xc1,
//    (internal) busy enabled (test pulse enabled), busy latch, TEST PULSE INPUT, internal busy out,
//    count triggers, disable external counter clear, pushbutton resets BIM
      INI_CSR1_TESTPULSE = 0xcc,
//    interrupt enable (IRE), autoclear, level 0
      INI_BIMCR0 = 0x18,
//    interrupt disable (level 0)
      DIS_BIMCR0 = 0x00
  };

   protected:
      virtual bool waitForTrigger();
   private:
      bool m_busy;			// trigger busy
      bool m_stopDCEntered;		
      bool m_stopDCDone;		

      int m_ini_bimcr0;

      int m_corboChannel;
      int m_interruptVector;
      VME_InterruptInfo_t m_irInfo;
      int m_int_handle;

   };
}
#endif
