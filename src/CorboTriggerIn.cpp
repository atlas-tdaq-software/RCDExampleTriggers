// $Id$
// //////////////////////////////////////////////////////////////////////
//  Simple TriggerIn to queue a HardwareTriggeredRequest every time 
//  there is an "interrupt" from the CORBO: a bit in a register (poll) or 
//  a VMEbus interrupt
//
//  Author: J.Petersen CERN 
//
// //////////////////////////////////////////////////////////////////////

#include "RCDExampleTriggers/CorboTriggerIn.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "RCDExampleTriggers/RCDTriggerException.h"
#include "DFDebug/DFDebug.h"
#include "rcc_corbo/rcc_corbo.h"
#include "rcc_corbo/rcc_corbo_int.h"
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

using namespace ROS;
/*****************************************************************************************/
void CorboTriggerIn::setup(IOManager* iomanager, DFCountedPointer<Config> configuration)
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 10, "CorboTriggerIn::setup entered");

  HardwareTriggerIn::setup(iomanager, configuration); 

  m_corboChannel = configuration->getInt("CorboChannelNumber");
  DEBUG_TEXT(DFDB_ROSIO, 10, "CorboTriggerIn::setup, CORBO channel = " << std::dec << m_corboChannel);

}

/***************************************/
void CorboTriggerIn::configure(const daq::rc::TransitionCmd&)
/***************************************/
{
  err_type ret;
  err_str rcc_err_str;

  DEBUG_TEXT(DFDB_ROSIO, 10, "CorboTriggerIn::configure entered");

  ret = VME_Open();
  if (ret != VME_SUCCESS) {
      DEBUG_TEXT(DFDB_ROSIO, 5, "CorboTriggerIn::configure: Error from VME_Open");	// FIXME ROSIO
      rcc_error_string(rcc_err_str, ret);
      CREATE_ROS_EXCEPTION(tException, RCDTriggerException, VME_OPEN, rcc_err_str);
      throw tException;
  }

  // CORBO
  ret = TM_open(c_vmeBaseCorbo);
  if( ret != CBE_OK && ret != CBE_ISOPEN) {
    DEBUG_TEXT(DFDB_ROSIO, 5, "CorboTriggerIn::configure: Error from TM_open");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(tException, RCDTriggerException, CORBO_OPEN, rcc_err_str);
    throw tException;
  }
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::configure: CORBO opened");

// clear from out to in, enable from in to out
  TM_csrW(m_corboChannel,DIS_CSR1);     // disable (int) busy eqv trigger i/p
  TM_clbsy(m_corboChannel);             // clear pending i/r
  TM_bimcrW(m_corboChannel,DIS_BIMCR0);

}

/*****************************************************************/
void CorboTriggerIn::unconfigure(const daq::rc::TransitionCmd&)
/*****************************************************************/
{
  err_type ret;
  err_str rcc_err_str;

  DEBUG_TEXT(DFDB_ROSIO, 10, "CorboTriggerIn::unconfigure entered");

  ret = TM_close();
  if( ret != CBE_OK) {
    DEBUG_TEXT(DFDB_ROSIO, 5, "CorboTriggerIn::unconfigure: Error from TM_Close");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION (tException, RCDTriggerException, CORBO_CLOSE, rcc_err_str);
    throw tException;
  }

  ret = VME_Close();
  if (ret != VME_SUCCESS) {
      DEBUG_TEXT(DFDB_ROSIO, 5, "CorboTriggerIn::unconfigure: Error from VME_Close");
      rcc_error_string(rcc_err_str, ret);
      CREATE_ROS_EXCEPTION (tException, RCDTriggerException, VME_CLOSE, rcc_err_str);
      throw tException;
  }

}

/*****************************************************************************************/
void CorboTriggerIn::prepareForRun(const daq::rc::TransitionCmd& cmd)
/*****************************************************************************************/
{
   DEBUG_TEXT(DFDB_ROSIO, 10, "CorboTriggerIn::prepareForRun entered");

  TM_csrW(m_corboChannel,INI_CSR1);             // (re)enable triggers
  m_busy = false;

  HardwareTriggerIn::prepareForRun(cmd);	// start trigger thread

}

/*****************************************************************/
void CorboTriggerIn::stopDC(const daq::rc::TransitionCmd&)
/*****************************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "CorboTriggerIn::stopDC: Entered");

// clear from out to in, enable from in to out
  TM_csrW(m_corboChannel,DIS_CSR1);     // disable (int) busy eqv trigger i/p
  TM_clbsy(m_corboChannel);             // clear pending i/r
  TM_bimcrW(m_corboChannel,DIS_BIMCR0);

  m_busy = true;
}

/*****************************************************************************************/
void CorboTriggerIn::clear()
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 10, "CorboTriggerIn::clear entered");

  TM_clbsy(m_corboChannel);		// clear poll bit
  m_busy = false;

}

/*****************************************************************************************/
bool CorboTriggerIn::waitForTrigger()
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 10, "CorboTriggerIn::waitForTrigger entered");

  int csrData;

  DEBUG_TEXT(DFDB_ROSIO, 20, "CorboTriggerIn::waitForTrigger:  m_busy = " << m_busy);

  if (m_busy) {
    return false;
  }
  else {
    TM_csrR(m_corboChannel,&csrData);
    DEBUG_TEXT(DFDB_ROSIO, 20, "CorboTriggerIn::waitForTrigger:  csrData = " << std::hex << csrData);

    if (csrData & CORBO_CSR_BSSTATE) {    // local BUSY present
      m_busy = true;
      return(true);
    }
    else {
      return (false);
    }
  }

}

/** Shared library entry point */
extern "C" {
   extern TriggerIn* createCorboTriggerIn();
}
TriggerIn* createCorboTriggerIn()
{
   return (new CorboTriggerIn());
}
