// $Id: CorboTriggerIn.cpp 117146 2013-10-09 11:30:27Z gcrone $
// //////////////////////////////////////////////////////////////////////
//  Simple TriggerIn to queue a HardwareTriggeredRequest every time 
//  there is an interrupt from the CORBO 
//  this version uses VME interrupts with NO timeout
//  14/02/21  possible race conditions between the control functions and the trigger functions:
//  executing in different threads. In real life: stop triggers before doing a STOP
//
//  Author: J.Petersen CERN 
//
// //////////////////////////////////////////////////////////////////////

#include "RCDExampleTriggers/CorboTriggerInInterruptNotimeout.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "RCDExampleTriggers/RCDTriggerException.h"
#include "DFDebug/DFDebug.h"
#include "rcc_corbo/rcc_corbo.h"
#include "rcc_corbo/rcc_corbo_int.h"
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

using namespace ROS;
/*****************************************************************************************/
void CorboTriggerInInterruptNotimeout::setup(IOManager* iomanager, DFCountedPointer<Config> configuration)
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::setup entered");

  HardwareTriggerIn::setup(iomanager, configuration); 

  m_corboChannel = configuration->getInt("CorboChannelNumber");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::setup, CORBO channel = " << std::dec << m_corboChannel);

  m_interruptVector = configuration->getInt("VMEbusInterruptVector");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, " CorboTriggerIn::setup: interrupt vector = " << m_interruptVector);

}

/**************************************************************/
void CorboTriggerInInterruptNotimeout::configure(const daq::rc::TransitionCmd&)
/**************************************************************/
{
  err_type ret;
  err_str rcc_err_str;
  VME_InterruptList_t irq_list;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::configure entered");

  ret = VME_Open();
  if (ret != VME_SUCCESS) {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::configure: Error from VME_Open");
      rcc_error_string(rcc_err_str, ret);
      CREATE_ROS_EXCEPTION(tException, RCDTriggerException, VME_OPEN, rcc_err_str);
      throw tException;
  }

  // CORBO
  ret = TM_open(c_vmeBaseCorbo);
  if( ret != CBE_OK && ret != CBE_ISOPEN) {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::configure: Error from TM_open");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(tException, RCDTriggerException, CORBO_OPEN, rcc_err_str);
    throw tException;
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::configure: CORBO opened");

  irq_list.number_of_items = 1;
  irq_list.list_of_items[0].vector = m_interruptVector; 
  irq_list.list_of_items[0].level = 3;		// hardwired
  m_ini_bimcr0 = INI_BIMCR0 + 3;    
  irq_list.list_of_items[0].type = VME_INT_ROAK;

// clear from out to in, enable from in to out
  TM_csrW(m_corboChannel,DIS_CSR1);     // disable (int) busy eqv trigger i/p
  TM_clbsy(m_corboChannel);             // clear pending i/r
  TM_bimcrW(m_corboChannel,DIS_BIMCR0);
  TM_bimvrW(m_corboChannel, irq_list.list_of_items[0].vector);
  TM_bimcrW(m_corboChannel, m_ini_bimcr0);

  ret = VME_InterruptLink(&irq_list, &m_int_handle);
  if (ret != VME_SUCCESS) {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::configure: Error from VME_InterruptLink");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(tException, RCDTriggerException, VME_INTERRUPTLINK, rcc_err_str);
    throw tException;
  }

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::configure done");

}

/************************************************************************************/
void CorboTriggerInInterruptNotimeout::unconfigure(const daq::rc::TransitionCmd&)
/************************************************************************************/
{
  err_type ret;
  err_str rcc_err_str;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::unconfigure entered");

  ret = VME_InterruptUnlink(m_int_handle);
  if (ret != VME_SUCCESS) {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::unconfigure: Error from VME_InterruptUnlink");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(tException, RCDTriggerException, VME_INTERRUPTUNLINK, rcc_err_str);
    throw tException;
  }

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::unconfigure: VME_InterruptUnlink");

  ret = TM_close();
  if( ret != CBE_OK) {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::unconfigure: Error from TM_Close");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION (tException, RCDTriggerException, CORBO_CLOSE, rcc_err_str);
    throw tException;
  }

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::unconfigure: TM_close");

  ret = VME_Close();
  if (ret != VME_SUCCESS) {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::unconfigure: Error from VME_Close");
      rcc_error_string(rcc_err_str, ret);
      CREATE_ROS_EXCEPTION (tException, RCDTriggerException, VME_CLOSE, rcc_err_str);
      throw tException;
  }

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::unconfigure: VME_close");

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::unconfigure done");
}

/*****************************************************************************************/
void CorboTriggerInInterruptNotimeout::prepareForRun(const daq::rc::TransitionCmd& cmd)
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::prepareForRun entered");

  m_irInfo.vector = 0;			// busy flag	
  m_stopDCEntered = false;
  m_stopDCDone = false;

  TM_csrW(m_corboChannel,DIS_CSR1);     // disable (int) busy eqv trigger i/p
  TM_clbsy(m_corboChannel);             // clear pending i/r
  TM_bimcrW(m_corboChannel,DIS_BIMCR0);
  TM_bimcrW(m_corboChannel, m_ini_bimcr0);	// reenable IR level 3, channel 1

  TM_csrW(m_corboChannel,INI_CSR1);     // (re)enable triggers

// should re-able physics triggers here!

  HardwareTriggerIn::prepareForRun(cmd);	// start trigger thread

}

// stopDC is asynchronous to the trigger thread.
// The code may be "hit" anywhere in the event loop. It may require the use of mutex'es and condition variables ..
// e.g. a CORBO mutex to protect the 'clear' sequence
// it seems that this can be avoided for the moment ...
/*****************************************************************************/
void CorboTriggerInInterruptNotimeout::stopDC(const daq::rc::TransitionCmd&)
/*****************************************************************************/
{

  int busyCount = 0;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "CorboTriggerIn::stopDC: Entered");

//  should stop physics triggers here!

  m_stopDCEntered = true;		// blocks re-enabling in clear()

  std::cout << "CorboTriggerIn::stopDC: m_irInfo.vector before last event loop = " << (int)m_irInfo.vector << std::endl;
  do {					// wait for clear() on the event
    usleep(1000);			// checked ~~ OK
    busyCount++;
  } while (m_irInfo.vector == m_interruptVector);			// or fall through after one wait if NONE

  std::cout << "CorboTriggerIn::stopDC: busy count = " << busyCount << std::endl;

// the CORBO is now "quiet"
  TM_csrW(m_corboChannel,INI_CSR1_TESTPULSE);     //enable test pulse and channel 
  TM_test(m_corboChannel);			// generate SW interrupt
						// system is ready for IRs with EXT interrrupts disabled.

  m_stopDCDone = true;

}

/*****************************************************************************************/
void CorboTriggerInInterruptNotimeout::clear()
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::clear entered");

  TM_csrW(m_corboChannel,DIS_CSR1);     // disable (int) busy eqv trigger i/p
  TM_clbsy(m_corboChannel);             // clear pending i/r
  TM_bimcrW(m_corboChannel,DIS_BIMCR0);
  TM_bimcrW(m_corboChannel, m_ini_bimcr0);	// check ME

  if(m_stopDCEntered == false) {		// don't enable if stopDC
    TM_csrW(m_corboChannel,INI_CSR1);           // (re)enable triggers
  }

  m_irInfo.vector = 0;

}

/*****************************************************************************************/
bool CorboTriggerInInterruptNotimeout::waitForTrigger()
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::waitForTrigger entered");

  err_type ret;
  err_str rcc_err_str;
  int csr;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "CorboTriggerIn::waitForTrigger:  m_stopDCEntered = " << m_stopDCEntered);

  if(m_stopDCDone) {		// don't wait if stopped (stopDC done)
    m_triggerActive = false;
    return false;
  }

// the busy flag irInfo.vector will be set in the IOCTL
  ret = VME_InterruptWait(m_int_handle, -1, &m_irInfo);	// NO timeout; NOT a cancellation point
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "CorboTriggerIn::waitForTrigger: Wait: vector = " << (int)m_irInfo.vector << " m_stopDCEntered = " << m_stopDCEntered);
  if (ret != VME_SUCCESS) {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "CorboTriggerIn::waitForTrigger: Error from VME_InterruptWait, ret = " << HEX(ret));
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION (tException, RCDTriggerException, VME_INTERRUPTWAIT, rcc_err_str);
    throw tException;
  }

  if (m_stopDCEntered == false) {			// normal trigger
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "CorboTriggerIn::waitForTrigger:  vector = " << (int)m_irInfo.vector);
    return true;
  }

// last event - should be soft IR
// a small possibility for stopDCEntered = true on real trigger ..
  std::cout << "CorboTriggerIn::waitForTrigger:  last trigger, vector = " << (int)m_irInfo.vector << std::endl;
  TM_csrR(m_corboChannel,&csr);     //read CSR
  csr = csr & 0xFF;
  std::cout << "CorboTriggerIn::waitForTrigger:  last trigger, csr = " << csr << " INI_CSR1_TESTPULSE = " << INI_CSR1_TESTPULSE << std::endl;
  if (csr == INI_CSR1_TESTPULSE) {	// soft IR
// no data. Clear and leave trigger disabled
    std::cout << "CorboTriggerIn::waitForTrigger:  last event: SW trigger " << std::endl;
    TM_csrW(m_corboChannel,DIS_CSR1);     // disable (int) busy eqv trigger i/p - redundant?
    TM_clbsy(m_corboChannel);             // clear pending i/r
    TM_bimcrW(m_corboChannel,DIS_BIMCR0);
    TM_bimcrW(m_corboChannel, m_ini_bimcr0);	// check ME

    return false;
  }
  else { 		// real trigger - exceptional ..
    std::cout << "CorboTriggerIn::waitForTrigger:last event: HW trigger " << std::endl;
    return true;
  }

}

/** Shared library entry point */
extern "C" {
   extern TriggerIn* createCorboTriggerInInterruptNotimeout();
}
TriggerIn* createCorboTriggerInInterruptNotimeout()
{
   return (new CorboTriggerInInterruptNotimeout());
}
