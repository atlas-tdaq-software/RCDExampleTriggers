// $Id$
// //////////////////////////////////////////////////////////////////////
//  Simple TriggerIn to queue a HardwareTriggeredRequest every time it
// reads a word from a socket.
//
//  Author:  G.J.Crone
//
//  $Log$
//  Revision 1.6  2006/11/02 10:16:21  gcrone
//  Update for new ERS based ROSExceptions
//
//  Revision 1.5  2006/03/10 12:20:10  gcrone
//  Rename stopFE to stopEB
//
//  Revision 1.4  2005/07/28 07:38:31  gcrone
//  Include DFDebug.h directly
//
//  Revision 1.3  2005/06/22 09:38:46  gcrone
//  Use RCDTriggerException instead of IOException.
//
//  Revision 1.2  2005/06/22 09:18:43  gcrone
//  fix include paths
//
//  Revision 1.1.1.1  2005/06/22 08:22:59  gcrone
//  new package
//
//  Revision 1.1  2005/05/27 09:03:26  gcrone
//  Added HardwareTriggerIn and related Requests
//
//
// //////////////////////////////////////////////////////////////////////

#include "RCDExampleTriggers/SocketTriggerIn.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "RCDExampleTriggers/RCDTriggerException.h"
#include "DFDebug/DFDebug.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

using namespace ROS;

void SocketTriggerIn::setup(IOManager* iomanager, DFCountedPointer<Config> configuration)
{
   // First call the corresponding setup from our base class
   HardwareTriggerIn::setup(iomanager, configuration);


   // Now do our specific setup

   m_port=configuration->getInt("Port");

   // Create a socket to listen on
   m_listenSocket = socket (AF_INET, SOCK_STREAM, 0) ;
   if (m_listenSocket == -1) {
      CREATE_ROS_EXCEPTION(tException, RCDTriggerException,SOCKET_CREATE_ERROR, "");
      throw tException;
   }

   // Avoid TIME_WAIT problems if we restart shortly after exiting
   int optval = 1 ;
   int status = setsockopt (m_listenSocket, SOL_SOCKET, SO_REUSEADDR,
                        &optval, sizeof (optval)) ;

   // Bind it to my port
   struct sockaddr_in myaddr ;
   myaddr.sin_port = htons (m_port) ;
   myaddr.sin_addr.s_addr = htonl (INADDR_ANY) ;
   status = bind (m_listenSocket, (struct sockaddr*) &myaddr, sizeof (myaddr)) ;
   if (status == -1) {
      CREATE_ROS_EXCEPTION(tException, RCDTriggerException, SOCKET_BIND_ERROR, "");
      throw tException;
   }

   // Set it to listen
   status = listen (m_listenSocket, 1) ;
   if (status == -1) {
      CREATE_ROS_EXCEPTION(tException, RCDTriggerException, SOCKET_LISTEN_ERROR, "");
      throw tException;
   }
}

void SocketTriggerIn::prepareForRun(const daq::rc::TransitionCmd&)
{
   struct sockaddr_in clientAddr ;
   int addrSize = sizeof (clientAddr) ;
   int newFile = accept (m_listenSocket,
                         (struct sockaddr*) &clientAddr,
                         (socklen_t*) &addrSize) ;
   if (newFile < 0) {
      CREATE_ROS_EXCEPTION (tException, RCDTriggerException, SOCKET_ACCEPT_ERROR, "");
      throw tException;
   }

   m_triggerSocket=newFile ;

   // Only used for the DEBUG_TEXT!!
   struct hostent *clienthent=gethostbyaddr ((char *)&clientAddr.sin_addr,
                                             sizeof(clientAddr.sin_addr),
                                             AF_INET) ;
   char *nameString ;
   if (clienthent != NULL) {
      nameString = clienthent->h_name ;
   }
   else {
      nameString = inet_ntoa (clientAddr.sin_addr) ;
   }
   std::string debugString("New connection from ");
   debugString+=nameString;
   DEBUG_TEXT(DFDB_ROSIO, 10, debugString.c_str()) ;
}

void SocketTriggerIn::stopGathering(const daq::rc::TransitionCmd& cmd)
{
   close(m_triggerSocket);
   HardwareTriggerIn::stopGathering(cmd);
}
void SocketTriggerIn::clear()
{
   DEBUG_TEXT(DFDB_ROSIO, 10, "SocketTriggerIn::clear entered");
   write(m_triggerSocket, "CLR\n", 4);
}
bool SocketTriggerIn::waitForTrigger()
{
   DEBUG_TEXT(DFDB_ROSIO, 10, "SocketTriggerIn::waitForTrigger entered");
   char* buffer[4];
   int nread=read(m_triggerSocket, &buffer, sizeof(buffer));
   if (nread <= 0) {
      close(m_triggerSocket);
      return false;
   }
   return true;
}

/** Shared library entry point */
extern "C" {
   extern TriggerIn* createSocketTriggerIn();
}
TriggerIn* createSocketTriggerIn()
{
   return (new SocketTriggerIn());
}
