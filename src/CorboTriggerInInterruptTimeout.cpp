// $Id: CorboTriggerIn.cpp 117146 2013-10-09 11:30:27Z gcrone $
// //////////////////////////////////////////////////////////////////////
//  Simple TriggerIn to queue a HardwareTriggeredRequest every time 
//  there is an interrupt from the CORBO 
//  This version is using a timeout on InterruptWait()
//  14/02/21  possible race conditions between the control functions and the trigger functions:
//  executing in different threads. In real life: stop triggers before doing a STOP
//
//  Author: J.Petersen CERN 
//
// //////////////////////////////////////////////////////////////////////

#include "RCDExampleTriggers/CorboTriggerInInterruptTimeout.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "RCDExampleTriggers/RCDTriggerException.h"
#include "DFDebug/DFDebug.h"
#include "rcc_corbo/rcc_corbo.h"
#include "rcc_corbo/rcc_corbo_int.h"
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

using namespace ROS;
/*****************************************************************************************/
void CorboTriggerInInterruptTimeout::setup(IOManager* iomanager, DFCountedPointer<Config> configuration)
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::setup entered");

  HardwareTriggerIn::setup(iomanager, configuration); 

  m_corboChannel = configuration->getInt("CorboChannelNumber");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::setup, CORBO channel = " << std::dec << m_corboChannel);

  m_interruptVector = configuration->getInt("VMEbusInterruptVector");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, " CorboTriggerIn::setup: interrupt vector = " << m_interruptVector);

}

/*******************************************************************************/
void CorboTriggerInInterruptTimeout::configure(const daq::rc::TransitionCmd&)
/*******************************************************************************/
{
  err_type ret;
  err_str rcc_err_str;
  VME_InterruptList_t irq_list;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::configure entered");

  ret = VME_Open();
  if (ret != VME_SUCCESS) {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::configure: Error from VME_Open");
      rcc_error_string(rcc_err_str, ret);
      CREATE_ROS_EXCEPTION(tException, RCDTriggerException, VME_OPEN, rcc_err_str);
      throw tException;
  }

  // CORBO
  ret = TM_open(c_vmeBaseCorbo);
  if( ret != CBE_OK && ret != CBE_ISOPEN) {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::configure: Error from TM_open");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(tException, RCDTriggerException, CORBO_OPEN, rcc_err_str);
    throw tException;
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::configure: CORBO opened");

  irq_list.number_of_items = 1;
  irq_list.list_of_items[0].vector = m_interruptVector;
  irq_list.list_of_items[0].level = 3;		// hardwired
  m_ini_bimcr0 = INI_BIMCR0 + 3;    
  irq_list.list_of_items[0].type = VME_INT_ROAK;

// protect the InterruptLink: clear from out to in
  TM_csrW(m_corboChannel,DIS_CSR1);     // disable (int) busy eqv trigger i/p
  TM_clbsy(m_corboChannel);             // clear pending i/r
  TM_bimcrW(m_corboChannel,DIS_BIMCR0);
  TM_bimvrW(m_corboChannel, irq_list.list_of_items[0].vector);
  TM_bimcrW(m_corboChannel, m_ini_bimcr0);

  ret = VME_InterruptLink(&irq_list, &m_int_handle);
  if (ret != VME_SUCCESS) {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::configure: Error from VME_InterruptLink");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(tException, RCDTriggerException, VME_INTERRUPTLINK, rcc_err_str);
    throw tException;
  }

}

/***********************************************************************************/
void CorboTriggerInInterruptTimeout::unconfigure(const daq::rc::TransitionCmd&)
/***********************************************************************************/
{
  err_type ret;
  err_str rcc_err_str;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::unconfigure entered");

  ret = VME_InterruptUnlink(m_int_handle);
  if (ret != VME_SUCCESS) {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::unconfigure: Error from VME_InterruptUnlink");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(tException, RCDTriggerException, VME_INTERRUPTUNLINK, rcc_err_str);
    throw tException;
  }

  ret = TM_close();
  if( ret != CBE_OK) {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::unconfigure: Error from TM_Close");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION (tException, RCDTriggerException, CORBO_CLOSE, rcc_err_str);
    throw tException;
  }

  ret = VME_Close();
  if (ret != VME_SUCCESS) {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::unconfigure: Error from VME_Close");
      rcc_error_string(rcc_err_str, ret);
      CREATE_ROS_EXCEPTION (tException, RCDTriggerException, VME_CLOSE, rcc_err_str);
      throw tException;
  }

}

/*****************************************************************************************/
void CorboTriggerInInterruptTimeout::prepareForRun(const daq::rc::TransitionCmd& cmd)
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::prepareForRun entered");

  TM_csrW(m_corboChannel,DIS_CSR1);     // disable (int) busy eqv trigger i/p
  TM_clbsy(m_corboChannel);             // clear pending i/r
  TM_bimcrW(m_corboChannel,DIS_BIMCR0);
  TM_bimcrW(m_corboChannel, m_ini_bimcr0);	// check ME
  TM_csrW(m_corboChannel,INI_CSR1);             // (re)enable triggers

  m_stop = false;
  m_timeouts = 0;
  m_timeoutAndStop = false;

  HardwareTriggerIn::prepareForRun(cmd);	// start trigger thread

}

// this function is running in parallel to the TriggerIn!! : possible race conditions
/**********************************************************************/
void CorboTriggerInInterruptTimeout::stopDC(const daq::rc::TransitionCmd&)
/**********************************************************************/
{
  int busyCount = 0;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "CorboTriggerIn::stopDC: Entered");

  m_stop = true;		// blocks clear()

// now wait for last event(s) and a timeout: thread switch! Expect to wait ~ 50 ms!
  std::cout << "  CorboTriggerIn::stopDC: timeoutAndStop before last event loop = " << m_timeoutAndStop << std::endl;
  do {
    usleep(1000);
    busyCount++;
  } while (m_timeoutAndStop == false);                     // or fall through after one wait if NONE

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "CorboTriggerIn::stopDC: busy count = " << busyCount);
  std::cout << "  CorboTriggerIn::stopDC: busy count = " << busyCount << std::endl;

}

/*****************************************************************************************/
void CorboTriggerInInterruptTimeout::clear()
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::clear entered");

  if (m_stop == false) {			  // ==> timeout
    TM_csrW(m_corboChannel,DIS_CSR1);     // disable (int) busy eqv trigger i/p
    TM_clbsy(m_corboChannel);             // clear pending i/r
    TM_bimcrW(m_corboChannel,DIS_BIMCR0);
    TM_bimcrW(m_corboChannel, m_ini_bimcr0);	// check ME

    TM_csrW(m_corboChannel,INI_CSR1);             // (re)enable triggers
  }
}

/*****************************************************************************************/
bool CorboTriggerInInterruptTimeout::waitForTrigger()
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 10, "CorboTriggerIn::waitForTrigger entered");

  err_type ret;
  err_str rcc_err_str;
  bool timeout;
  VME_InterruptInfo_t ir_info;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "CorboTriggerIn::waitForTrigger:  m_stop = " << m_stop);

  do {							// loop on real timeouts
    timeout = false;

    ret = VME_InterruptWait(m_int_handle, 100, &ir_info);	// timeout = 100 ms; NOT a cancellation point
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "CorboTriggerIn::waitForTrigger: Wait: vector = " << (int)ir_info.vector << " m_stop = " << m_stop);
    if (ret != VME_SUCCESS) {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "CorboTriggerIn::waitForTrigger: Error from VME_InterruptWait, ret = " << HEX(ret));
      if (RCC_ERROR_MAJOR(ret) == VME_TIMEOUT) {
        timeout = true;
        m_timeouts++;
        DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::WaitForTrigger: Timeout");
        if (m_stop) {			// stop occurred .. exit timeout loop
          DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "CorboTriggerIn::waitForTrigger: timeout & stop ");
          m_timeoutAndStop = true;
          std::cout << " # timeouts in run = " << m_timeouts << std::endl;	// -> is_info
          m_triggerActive = false;						// stop loop in HWTriggerIn
          return false;
        }
      }
      else {
        DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "CorboTriggerIn::WaitForTrigger: Error from VME_InterruptWait");
        rcc_error_string(rcc_err_str, ret);
        CREATE_ROS_EXCEPTION (tException, RCDTriggerException, VME_INTERRUPTWAIT, rcc_err_str);
        throw tException;
      }
    }
  } while(timeout);

// real interrupt
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "CorboTriggerIn::waitForTrigger:  vector = " << (int)ir_info.vector);
    return true;

}

/** Shared library entry point */
extern "C" {
   extern TriggerIn* createCorboTriggerInInterruptTimeout();
}
TriggerIn* createCorboTriggerInInterruptTimeout()
{
   return (new CorboTriggerInInterruptTimeout());
}
